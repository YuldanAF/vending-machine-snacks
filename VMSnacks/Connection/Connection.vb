﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Configuration
Module Connection
    Public Connect As SqlConnection
    Public Datas As DataSet
    Public Read As SqlDataReader
    Public Adapter As SqlDataAdapter
    Public Command As SqlCommand
    Public DT As DataTable
    Public Query As String

    Public Sub OpenConnection()

        Query = ConfigurationManager.ConnectionStrings("ConString").ConnectionString
        Connect = New SqlConnection(Query)
        Try
            If Connect.State = ConnectionState.Closed Then
                Connect.Open()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Module
