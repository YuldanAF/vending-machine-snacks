﻿Imports System.Data
Imports System.Data.SqlClient
Public Class BucketControl
    Sub InsertBucket(ByVal obj As Bucket)
        Try
            Connect.Close()
            OpenConnection()
            Dim str As String
            Dim Command As New SqlCommand("INSERT INTO
            Bucket(Food,Prices,Qty,AmountDue,Status) VALUES
            (@food,@prices,@qty,@amountdue,@status)", Connect)
            Command.Parameters.AddWithValue("@food", obj.Food)
            Command.Parameters.AddWithValue("@prices", obj.Prices)
            Command.Parameters.AddWithValue("@qty", obj.Qty)
            Command.Parameters.AddWithValue("@amountdue", obj.AmountDue)
            Command.Parameters.AddWithValue("@status", "NotPaid")
            Command.ExecuteNonQuery()
            Command.ExecuteNonQuery()
            MessageBox.Show("Add Food To Bucket is Succed")

        Catch ex As Exception
            MessageBox.Show("Add Food To Bucket is Failed")
        End Try
    End Sub


End Class
