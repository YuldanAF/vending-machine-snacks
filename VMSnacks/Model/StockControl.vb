﻿Imports System.Data
Imports System.Data.SqlClient
Public Class StockControl
    Public Sub UpdateStock(ByVal obj As Stock)
        Connect.Close()
        OpenConnection()
        Dim Command As New SqlCommand("UPDATE Stock SET
Qty=@qty WHERE
Name=@name", Connect)
        Command.Parameters.AddWithValue("@qty", obj.Qty)
        Command.Parameters.AddWithValue("@name", obj.Name)
        Command.ExecuteNonQuery()
        Connect.Close()
    End Sub

End Class
