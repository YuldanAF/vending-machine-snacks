﻿Imports System.Data
Imports System.Data.SqlClient
Public Class StockJoinControl
    Public Function ReadStockJoin(ByVal ID As String) As StockJoin
        Dim Obj As New StockJoin
        Connect.Close()
        OpenConnection()
        Dim Command As New SqlCommand("select Food.Name,Prices,Qty from Food inner Join Stock on Stock.Name = Food.Name where Food.Name=@id",
Connect)
        Command.Parameters.AddWithValue("@id", ID)
        Dim reader As SqlDataReader = Command.ExecuteReader
        If reader.Read Then
            Obj.Name = reader("Name")
            Obj.Prices = reader("Prices")
            Obj.Qty = reader("Qty")
        End If
        Return Obj
        Connect.Close()
    End Function

End Class
