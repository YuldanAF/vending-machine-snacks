﻿Public Class Stock
    Private FoodName As String
    Private QtyFood As String


    Public Property Name As String
        Get
            Return FoodName
        End Get
        Set(value As String)
            FoodName = value
        End Set
    End Property

    Public Property Qty As String
        Get
            Return QtyFood
        End Get
        Set(value As String)
            QtyFood = value
        End Set
    End Property

End Class
