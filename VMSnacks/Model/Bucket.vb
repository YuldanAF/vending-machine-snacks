﻿Public Class Bucket
    Private Ida As Int64
    Private FoodName As String
    Private PricesFood As String
    Private QtyFood As String
    Private AmountDueFood As String
    Private StatusBucket As String
    Public Property ID As Int64
        Get
            Return ID
        End Get
        Set(value As Int64)
            Ida = value
        End Set
    End Property
    Public Property Food As String
        Get
            Return FoodName
        End Get
        Set(value As String)
            FoodName = value
        End Set
    End Property
    Public Property Prices As String
        Get
            Return PricesFood
        End Get
        Set(value As String)
            PricesFood = value
        End Set
    End Property
    Public Property Qty As String
        Get
            Return QtyFood
        End Get
        Set(value As String)
            QtyFood = value
        End Set
    End Property
    Public Property AmountDue As String
        Get
            Return AmountDueFood
        End Get
        Set(value As String)
            AmountDueFood = value
        End Set
    End Property
    Public Property Status As String
        Get
            Return StatusBucket
        End Get
        Set(value As String)
            StatusBucket = value
        End Set
    End Property

End Class
