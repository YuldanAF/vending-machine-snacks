﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class MainVending
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        OpenConnection()
    End Sub
    Private Sub Biscuits_Click(sender As Object, e As EventArgs) Handles Biscuits.Click
        Clean()
        Try
            Dim StockJoinModel As New StockJoin
            Dim StockJoinModelControl As New StockJoinControl
            Dim data As String = "Biskuit"

            FoodNameTextBox.Text = StockJoinModelControl.ReadStockJoin(data).Name
            PricesTextBox.Text = StockJoinModelControl.ReadStockJoin(data).Prices
            StockLabel.Text = StockJoinModelControl.ReadStockJoin(data).Qty
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Connect.Dispose()
        End Try
    End Sub
    Private Sub Chips_Click(sender As Object, e As EventArgs) Handles Chips.Click
        Clean()
        Try
            Dim StockJoinModel As New StockJoin
            Dim StockJoinModelControl As New StockJoinControl
            Dim data As String = "Chips"

            FoodNameTextBox.Text = StockJoinModelControl.ReadStockJoin(data).Name
            PricesTextBox.Text = StockJoinModelControl.ReadStockJoin(data).Prices
            StockLabel.Text = StockJoinModelControl.ReadStockJoin(data).Qty
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Connect.Dispose()
        End Try
    End Sub
    Private Sub Oreo_Click(sender As Object, e As EventArgs) Handles Oreo.Click
        Clean()
        Try
            Dim StockJoinModel As New StockJoin
            Dim StockJoinModelControl As New StockJoinControl
            Dim data As String = "Oreo"

            FoodNameTextBox.Text = StockJoinModelControl.ReadStockJoin(data).Name
            PricesTextBox.Text = StockJoinModelControl.ReadStockJoin(data).Prices
            StockLabel.Text = StockJoinModelControl.ReadStockJoin(data).Qty
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Connect.Dispose()
        End Try
    End Sub
    Private Sub Tango_Click(sender As Object, e As EventArgs) Handles Tango.Click
        Clean()
        Try
            Dim StockJoinModel As New StockJoin
            Dim StockJoinModelControl As New StockJoinControl
            Dim data As String = "Tango"

            FoodNameTextBox.Text = StockJoinModelControl.ReadStockJoin(data).Name
            PricesTextBox.Text = StockJoinModelControl.ReadStockJoin(data).Prices
            StockLabel.Text = StockJoinModelControl.ReadStockJoin(data).Qty
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Connect.Dispose()
        End Try
    End Sub
    Private Sub Cokelat_Click(sender As Object, e As EventArgs) Handles Cokelat.Click
        Clean()
        Try
            Dim StockJoinModel As New StockJoin
            Dim StockJoinModelControl As New StockJoinControl
            Dim data As String = "Cokelat"

            FoodNameTextBox.Text = StockJoinModelControl.ReadStockJoin(data).Name
            PricesTextBox.Text = StockJoinModelControl.ReadStockJoin(data).Prices
            StockLabel.Text = StockJoinModelControl.ReadStockJoin(data).Qty
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Connect.Dispose()
        End Try
    End Sub
    Private Sub AddBucket_Click(sender As Object, e As EventArgs) Handles AddBucket.Click
        Dim FoodStock As String = StockLabel.Text
        Dim FoodRequest As String = QtyTextBox.Text
        Dim FoodStockConvert As Double = 0
        Dim FoodRequestConvert As Double = 0
        Dim FoodLastStock As Double = 0
        Dim BucketModelControl As New BucketControl
        Dim BucketModel As New Bucket
        Dim StockModelControl As New StockControl
        Dim StockModel As New Stock
        FoodStockConvert = CDbl(Val(FoodStock))
        FoodRequestConvert = CDbl(Val(FoodRequest))

        If FoodStockConvert < FoodRequestConvert Then
            MsgBox("Insufficient Food Stock", MsgBoxStyle.Critical)
        Else
            FoodLastStock = FoodStockConvert - FoodRequestConvert
            BucketModel.Food = FoodNameTextBox.Text
            BucketModel.Prices = PricesTextBox.Text
            BucketModel.Qty = QtyTextBox.Text
            BucketModel.AmountDue = AmountDueTextBox.Text
            BucketModelControl.InsertBucket(BucketModel)
            StockModel.Qty = FoodLastStock
            StockModel.Name = FoodNameTextBox.Text
            StockModelControl.UpdateStock(StockModel)
            MsgBox("Add " + FoodRequestConvert.ToString() + " " + FoodNameTextBox.Text + "'s " + "To Bucket", MsgBoxStyle.Information)
        End If
        Clean()
    End Sub
    Sub Clean()
        FoodNameTextBox.Text = ""
        PricesTextBox.Text = ""
        QtyTextBox.Text = ""
        StockLabel.Text = 0
        AmountDueTextBox.Text = ""
    End Sub
    Private Sub Purchase_Click(sender As Object, e As EventArgs) Handles Purchase.Click
        MoneyPurchase.ShowDialog()
    End Sub
    Private Sub Cancel_Click(sender As Object, e As EventArgs) Handles Cancel.Click
        Clean()
    End Sub
    Private Sub QtyTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles QtyTextBox.KeyPress
        Dim QtyFood As String = QtyTextBox.Text
        Dim PricesFood As String = PricesTextBox.Text
        Dim TotalPrices As Double = 0
        TotalPrices = CDbl(Val(QtyFood) * Val(PricesFood)) '// Val do hard work
        AmountDueTextBox.Text = TotalPrices.ToString()
    End Sub
End Class
