﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainVending
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainVending))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Cokelat = New System.Windows.Forms.PictureBox()
        Me.Tango = New System.Windows.Forms.PictureBox()
        Me.Oreo = New System.Windows.Forms.PictureBox()
        Me.Chips = New System.Windows.Forms.PictureBox()
        Me.Biscuits = New System.Windows.Forms.PictureBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.StockLabel = New System.Windows.Forms.Label()
        Me.FoodNameTextBox = New System.Windows.Forms.TextBox()
        Me.PricesTextBox = New System.Windows.Forms.TextBox()
        Me.QtyTextBox = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.AddBucket = New System.Windows.Forms.Button()
        Me.Purchase = New System.Windows.Forms.Button()
        Me.Cancel = New System.Windows.Forms.Button()
        Me.AmountDueTextBox = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.Cokelat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Tango, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Oreo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Chips, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Biscuits, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Cokelat)
        Me.GroupBox1.Controls.Add(Me.Tango)
        Me.GroupBox1.Controls.Add(Me.Oreo)
        Me.GroupBox1.Controls.Add(Me.Chips)
        Me.GroupBox1.Controls.Add(Me.Biscuits)
        Me.GroupBox1.Location = New System.Drawing.Point(10, 10)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(420, 247)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "SnackBox"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label5.Location = New System.Drawing.Point(212, 222)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(98, 21)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Rp.15.000"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label4.Location = New System.Drawing.Point(82, 222)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 21)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Rp.12.000"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label3.Location = New System.Drawing.Point(282, 108)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 21)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Rp.10.000"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label2.Location = New System.Drawing.Point(159, 108)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 21)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Rp.8.000"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label1.Location = New System.Drawing.Point(30, 108)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 21)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Rp.6.000"
        '
        'Cokelat
        '
        Me.Cokelat.Image = CType(resources.GetObject("Cokelat.Image"), System.Drawing.Image)
        Me.Cokelat.Location = New System.Drawing.Point(201, 133)
        Me.Cokelat.Name = "Cokelat"
        Me.Cokelat.Size = New System.Drawing.Size(123, 86)
        Me.Cokelat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Cokelat.TabIndex = 4
        Me.Cokelat.TabStop = False
        '
        'Tango
        '
        Me.Tango.Image = CType(resources.GetObject("Tango.Image"), System.Drawing.Image)
        Me.Tango.Location = New System.Drawing.Point(72, 133)
        Me.Tango.Name = "Tango"
        Me.Tango.Size = New System.Drawing.Size(123, 86)
        Me.Tango.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Tango.TabIndex = 3
        Me.Tango.TabStop = False
        '
        'Oreo
        '
        Me.Oreo.Image = CType(resources.GetObject("Oreo.Image"), System.Drawing.Image)
        Me.Oreo.Location = New System.Drawing.Point(271, 20)
        Me.Oreo.Name = "Oreo"
        Me.Oreo.Size = New System.Drawing.Size(123, 86)
        Me.Oreo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Oreo.TabIndex = 2
        Me.Oreo.TabStop = False
        '
        'Chips
        '
        Me.Chips.Image = CType(resources.GetObject("Chips.Image"), System.Drawing.Image)
        Me.Chips.Location = New System.Drawing.Point(142, 20)
        Me.Chips.Name = "Chips"
        Me.Chips.Size = New System.Drawing.Size(123, 86)
        Me.Chips.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Chips.TabIndex = 1
        Me.Chips.TabStop = False
        '
        'Biscuits
        '
        Me.Biscuits.Image = CType(resources.GetObject("Biscuits.Image"), System.Drawing.Image)
        Me.Biscuits.Location = New System.Drawing.Point(14, 20)
        Me.Biscuits.Name = "Biscuits"
        Me.Biscuits.Size = New System.Drawing.Size(123, 86)
        Me.Biscuits.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Biscuits.TabIndex = 0
        Me.Biscuits.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Lucida Handwriting", 9.75!)
        Me.Label6.Location = New System.Drawing.Point(10, 260)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(376, 17)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Note : Please choose your food through the picture"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label7.Location = New System.Drawing.Point(433, 19)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(70, 21)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Food   "
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label8.Location = New System.Drawing.Point(431, 48)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 21)
        Me.Label8.TabIndex = 15
        Me.Label8.Text = "Prices "
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label9.Location = New System.Drawing.Point(433, 78)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(65, 21)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Qty     "
        '
        'StockLabel
        '
        Me.StockLabel.AutoSize = True
        Me.StockLabel.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.StockLabel.Location = New System.Drawing.Point(621, 232)
        Me.StockLabel.Name = "StockLabel"
        Me.StockLabel.Size = New System.Drawing.Size(56, 21)
        Me.StockLabel.TabIndex = 17
        Me.StockLabel.Text = "Stock"
        Me.StockLabel.Visible = False
        '
        'FoodNameTextBox
        '
        Me.FoodNameTextBox.Enabled = False
        Me.FoodNameTextBox.Location = New System.Drawing.Point(511, 17)
        Me.FoodNameTextBox.Name = "FoodNameTextBox"
        Me.FoodNameTextBox.Size = New System.Drawing.Size(139, 20)
        Me.FoodNameTextBox.TabIndex = 18
        '
        'PricesTextBox
        '
        Me.PricesTextBox.Enabled = False
        Me.PricesTextBox.Location = New System.Drawing.Point(510, 48)
        Me.PricesTextBox.Name = "PricesTextBox"
        Me.PricesTextBox.Size = New System.Drawing.Size(139, 20)
        Me.PricesTextBox.TabIndex = 19
        '
        'QtyTextBox
        '
        Me.QtyTextBox.Location = New System.Drawing.Point(510, 79)
        Me.QtyTextBox.Name = "QtyTextBox"
        Me.QtyTextBox.Size = New System.Drawing.Size(139, 20)
        Me.QtyTextBox.TabIndex = 20
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Lucida Handwriting", 9.75!)
        Me.Label11.Location = New System.Drawing.Point(435, 107)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(67, 34)
        Me.Label11.TabIndex = 21
        Me.Label11.Text = "Amount" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Due"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Lucida Handwriting", 9.75!)
        Me.Label12.Location = New System.Drawing.Point(494, 101)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(8, 17)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'AddBucket
        '
        Me.AddBucket.Location = New System.Drawing.Point(434, 158)
        Me.AddBucket.Name = "AddBucket"
        Me.AddBucket.Size = New System.Drawing.Size(64, 40)
        Me.AddBucket.TabIndex = 23
        Me.AddBucket.Text = "Add To Bucket"
        Me.AddBucket.UseVisualStyleBackColor = True
        '
        'Purchase
        '
        Me.Purchase.Location = New System.Drawing.Point(505, 158)
        Me.Purchase.Name = "Purchase"
        Me.Purchase.Size = New System.Drawing.Size(83, 40)
        Me.Purchase.TabIndex = 24
        Me.Purchase.Text = "Complete Purchase"
        Me.Purchase.UseVisualStyleBackColor = True
        '
        'Cancel
        '
        Me.Cancel.Location = New System.Drawing.Point(594, 158)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Size = New System.Drawing.Size(83, 40)
        Me.Cancel.TabIndex = 25
        Me.Cancel.Text = "Cancel"
        Me.Cancel.UseVisualStyleBackColor = True
        '
        'AmountDueTextBox
        '
        Me.AmountDueTextBox.Enabled = False
        Me.AmountDueTextBox.Location = New System.Drawing.Point(510, 109)
        Me.AmountDueTextBox.Name = "AmountDueTextBox"
        Me.AmountDueTextBox.Size = New System.Drawing.Size(139, 20)
        Me.AmountDueTextBox.TabIndex = 26
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label13.Location = New System.Drawing.Point(494, 19)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(15, 21)
        Me.Label13.TabIndex = 27
        Me.Label13.Text = ":"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label14.Location = New System.Drawing.Point(495, 47)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(15, 21)
        Me.Label14.TabIndex = 28
        Me.Label14.Text = ":"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label15.Location = New System.Drawing.Point(495, 78)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(15, 21)
        Me.Label15.TabIndex = 29
        Me.Label15.Text = ":"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label16.Location = New System.Drawing.Point(494, 109)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(15, 21)
        Me.Label16.TabIndex = 30
        Me.Label16.Text = ":"
        '
        'MainVending
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(686, 281)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.AmountDueTextBox)
        Me.Controls.Add(Me.Cancel)
        Me.Controls.Add(Me.Purchase)
        Me.Controls.Add(Me.AddBucket)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.QtyTextBox)
        Me.Controls.Add(Me.PricesTextBox)
        Me.Controls.Add(Me.FoodNameTextBox)
        Me.Controls.Add(Me.StockLabel)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "MainVending"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "VendingSnacks"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.Cokelat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Tango, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Oreo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Chips, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Biscuits, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Cokelat As PictureBox
    Friend WithEvents Tango As PictureBox
    Friend WithEvents Oreo As PictureBox
    Friend WithEvents Chips As PictureBox
    Friend WithEvents Biscuits As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents StockLabel As Label
    Friend WithEvents FoodNameTextBox As TextBox
    Friend WithEvents PricesTextBox As TextBox
    Friend WithEvents QtyTextBox As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents AddBucket As Button
    Friend WithEvents Purchase As Button
    Friend WithEvents Cancel As Button
    Friend WithEvents AmountDueTextBox As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
End Class
