﻿Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class MoneyPurchase
    Sub TotalPurchase()
        Try
            Connect.Close()
            OpenConnection()
            Dim query As String
            query = "select sum(Amountdue)as Total from Bucket where Status='NotPaid'"
            Command = New SqlCommand(query, Connect)

            Using Read As SqlDataReader = Command.ExecuteReader()
                Read.Read()
                TotalPurchaseTextBox.Text = Read("Total").ToString()
            End Using
            Connect.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Connect.Dispose()
        End Try
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim BucketModelControl As BucketControl
        If (String.IsNullOrEmpty(TotalMoneyTextBox.Text)) Then
            MsgBox("Please Insert Your Money Before Payment", MsgBoxStyle.Critical)
        Else
            Dim TotalPurchase As String = TotalPurchaseTextBox.Text
            Dim TotalMoney As String = TotalMoneyTextBox.Text
            Dim TotalPurchaseConvert As Double = 0
            Dim TotalMoneyConvert As Double = 0

            TotalPurchaseConvert = CDbl(Val(TotalPurchase))
            TotalMoneyConvert = CDbl(Val(TotalMoney))
            Dim ReturnMoney As Double = 0
            If TotalMoneyConvert < TotalPurchaseConvert Then
                MsgBox("Insufficient Money", MsgBoxStyle.Critical)
            Else
                UpdateStatus()
                ReturnMoney = TotalMoneyConvert - TotalPurchaseConvert
                MsgBox("Your remaining money is Rp." + ReturnMoney.ToString(), MsgBoxStyle.Information)
                MsgBox("Thanks For Your Transaction,Enjoy Your Day :)", MsgBoxStyle.Information)
                Clean()
                Me.Hide()
            End If
        End If

    End Sub
    Private Sub MoneyPurchase_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TotalPurchase()
    End Sub
    Sub Clean()
        TotalPurchaseTextBox.Text = ""
        TotalMoneyTextBox.Text = ""
    End Sub
    Public Sub UpdateStatus()
        Connect.Close()
        OpenConnection()
        Dim Command As New SqlCommand("UPDATE Bucket SET
Status='Paid' WHERE
Status='NotPaid'", Connect)
        Command.ExecuteNonQuery()
        Connect.Close()
    End Sub
End Class