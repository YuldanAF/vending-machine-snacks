﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MoneyPurchase
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TotalMoneyTextBox = New System.Windows.Forms.TextBox()
        Me.TotalPurchaseTextBox = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label7.Location = New System.Drawing.Point(88, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(101, 21)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Your Bill's"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label14.Location = New System.Drawing.Point(103, 103)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(15, 21)
        Me.Label14.TabIndex = 34
        Me.Label14.Text = ":"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label13.Location = New System.Drawing.Point(103, 51)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(15, 21)
        Me.Label13.TabIndex = 33
        Me.Label13.Text = ":"
        '
        'TotalMoneyTextBox
        '
        Me.TotalMoneyTextBox.Location = New System.Drawing.Point(133, 103)
        Me.TotalMoneyTextBox.Name = "TotalMoneyTextBox"
        Me.TotalMoneyTextBox.Size = New System.Drawing.Size(139, 20)
        Me.TotalMoneyTextBox.TabIndex = 32
        '
        'TotalPurchaseTextBox
        '
        Me.TotalPurchaseTextBox.Enabled = False
        Me.TotalPurchaseTextBox.Location = New System.Drawing.Point(133, 52)
        Me.TotalPurchaseTextBox.Name = "TotalPurchaseTextBox"
        Me.TotalPurchaseTextBox.Size = New System.Drawing.Size(139, 20)
        Me.TotalPurchaseTextBox.TabIndex = 31
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label8.Location = New System.Drawing.Point(14, 94)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(85, 42)
        Me.Label8.TabIndex = 30
        Me.Label8.Text = "Amount" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " Entered"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lucida Handwriting", 12.0!)
        Me.Label1.Location = New System.Drawing.Point(14, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 42)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Total " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Amount"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(92, 163)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(83, 40)
        Me.Button2.TabIndex = 35
        Me.Button2.Text = "Complete Purchase"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'MoneyPurchase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.TotalMoneyTextBox)
        Me.Controls.Add(Me.TotalPurchaseTextBox)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label7)
        Me.Name = "MoneyPurchase"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MoneyPurchase"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label7 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents TotalMoneyTextBox As TextBox
    Friend WithEvents TotalPurchaseTextBox As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Button2 As Button
End Class
